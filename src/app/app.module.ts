import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AnimalCardComponent } from './animal/components/animal-card/animal-card.component';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { AnimalPageComponent } from './animal/components/animal-page/animal-page.component';
import { AppRoutingModule } from './app.router.module';
import { VisitorPageComponent } from './visitor/components/visitor-page/visitor-page.component';
import { SponsorAnimalComponent } from './visitor/components/sponsor-animal/sponsor-animal.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ // Expose component over here
    AppComponent,
    AnimalCardComponent,
    AnimalPageComponent,
    VisitorPageComponent,
    SponsorAnimalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
