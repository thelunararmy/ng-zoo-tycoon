import { Injectable } from '@angular/core';
import { Animal } from '../models/animal.model';
import { FetchPurchaseableService } from './fetch-purchaseable.service';

@Injectable({
  providedIn: 'root'
})
export class OwnedAnimalsService {

  private _owned: Animal[] = [];

  get owned(): Animal[] {
    return this._owned;
  }

  public add_owned( new_animal : Animal ) : void {
    // Ensure animal is unique before it gets added
    if ( this._owned.filter( item => item.id == new_animal.id ).length === 0 ){
      this._owned.push( new_animal )
    } else {
      alert("Hey! We dont allow duplicated in my zoo!")
    }
  }

  public update_value( id : number, value: number ){
    this._owned.filter( item => item.id == id )[0].value += value;
  }

  constructor( private readonly fetchPurchasable : FetchPurchaseableService ) { 
    this.fetchPurchasable.getNewPurchaseable(1).subscribe( 
      (animals) => { this._owned.push(animals[0]) },
      (error) => { console.error("Could not add starting animal", error) },
    )
   }
}
