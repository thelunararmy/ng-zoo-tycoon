import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Animal } from '../models/animal.model';
import { Raw } from '../models/raw.model';

@Injectable({
  providedIn: 'root'
})
export class FetchPurchaseableService {

  constructor(private readonly http: HttpClient) { }
  
  getNewPurchaseable ( count : number = 1 ) : Observable<Animal[]> {
    return this.http.get<Raw[]>(`https://zoo-animal-api.herokuapp.com/animals/rand/${count}`)
    .pipe(
      map( raws => {
        return raws.map( item => ({
          name: item.name,
          picture: item.image_link,
          id: item.id,
          value: Array.from(item.name).map(letter => letter.charCodeAt(0)).reduce((a,b)=>a + b,0)
        }))
      })
    )
  }  
  
  
  debug_test () : string {
    return "Hello from FetchPurchaseableService";
  }

}
