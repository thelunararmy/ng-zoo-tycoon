import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Animal } from "../../models/animal.model";

@Component({
    selector: 'animal-card',
    templateUrl: './animal-card.component.html',
    styleUrls: [ './animal-card.component.css' ]
})
export class AnimalCardComponent {

    @Input() animal?: Animal;
    @Output() selected : EventEmitter<number> = new EventEmitter();

    handleOnPurchase (event: any): void {
        // console.log(event.target.id)
        this.selected.emit(event.target.id)
    }
}