import { Component, OnInit } from '@angular/core';
import { Animal } from '../../models/animal.model';
import { Raw } from '../../models/raw.model';
import { FetchPurchaseableService } from '../../services/fetch-purchaseable.service';
import { OwnedAnimalsService } from '../../services/owned-animals.service';

@Component({
  selector: 'app-animal-page',
  templateUrl: './animal-page.component.html',
  styleUrls: ['./animal-page.component.css']
})
export class AnimalPageComponent implements OnInit {

  constructor(
    private readonly fetchPurchaseable : FetchPurchaseableService,
    private readonly ownedAnimals : OwnedAnimalsService
    ) { }

  get owned() : Animal[] {
    return this.ownedAnimals.owned
  }

  ngOnInit(): void {
    // console.log('app-animal-page',this.fetchPurchaseable.debug_test());
    this.fetchPurchaseable.getNewPurchaseable(5)
    .subscribe( // success => {}, error => {}
      ( animals : Animal[] ) => { 
        animals.map( item => this.purchaseable.push( item ) ) 
      },
      (error : any) => { 
        console.error("Problem fetching new animals", error) 
      }
    ) 
  }

  handleSelectedEvent (id:number) : void {
    // console.log("Animal purchased:",id)
    const purchased_animal = this.purchaseable.filter(a => a.id == id)[0]
    this.ownedAnimals.add_owned( purchased_animal )
    this.purchaseable = this.purchaseable.filter(a => a.id != id)
  }

  public purchaseable : Animal[] = []

}
