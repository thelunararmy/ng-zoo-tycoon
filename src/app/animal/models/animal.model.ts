export interface Animal {
    name: string;
    picture: string;
    value: number;
    id: number;
}
