import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AnimalPageComponent } from "./animal/components/animal-page/animal-page.component";
import { VisitorPageComponent } from "./visitor/components/visitor-page/visitor-page.component";

const routes : Routes = [
    { path: "animals", component: AnimalPageComponent },
    { path: "visitor", component: VisitorPageComponent },
    { path: "", pathMatch: 'full', redirectTo:"/visitor" }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule{}