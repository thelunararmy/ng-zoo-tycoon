import { Component } from '@angular/core';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = '🐘🦆 Zoo Tycoon 🦒🦅';

  handleLoginKeycloak (): void {
    keycloak.login()
  }

  handleLogoutKeycloak (): void {
    keycloak.logout()
  }

  get token () : string | undefined {
    return keycloak.token
  }

  username () : string | undefined  {
    if (keycloak.authenticated){
      return `${keycloak.tokenParsed?.given_name} ${keycloak.tokenParsed?.family_name}` 
    } else {
      return ""
    }
  }

  get loggedIn () : Boolean | undefined {
    return keycloak.authenticated;
  }

}

