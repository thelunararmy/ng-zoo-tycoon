import { Component, OnInit } from '@angular/core';
import { Animal } from 'src/app/animal/models/animal.model';
import { OwnedAnimalsService } from 'src/app/animal/services/owned-animals.service';

@Component({
  selector: 'app-visitor-page',
  templateUrl: './visitor-page.component.html',
  styleUrls: ['./visitor-page.component.css']
})
export class VisitorPageComponent implements OnInit {

  constructor(private readonly ownedAnimals: OwnedAnimalsService) { }

  get owned() : Animal[][] { // [{},{},{},{}] -> [ [{},{},{}]. [{},{},{}] ]
    const animals: Animal[] = this.ownedAnimals.owned
    const result: Animal[][] = [];
    const grouping = 3;
    for (let i = 0; i < animals.length; i += grouping) {
      result.push( animals.slice(i, i+grouping) )
    }
    return result
  }

  ngOnInit(): void {
  }

}
