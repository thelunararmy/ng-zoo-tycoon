import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Animal } from 'src/app/animal/models/animal.model';
import { OwnedAnimalsService } from 'src/app/animal/services/owned-animals.service';

@Component({
  selector: 'app-sponsor-animal',
  templateUrl: './sponsor-animal.component.html',
  styleUrls: ['./sponsor-animal.component.css']
})
export class SponsorAnimalComponent implements OnInit {

  constructor(private readonly ownedAnimals : OwnedAnimalsService) { }

  ngOnInit(): void {
  }

  get owned() : Animal[] {
    return this.ownedAnimals.owned
  }

  public onSubmit(form: NgForm) : void {
    // console.log("form.value",form.value)
    let { amount, id } = form.value
    amount = amount < 0 ? -amount : amount 
    if (this.owned.length > 0) {
      this.ownedAnimals.update_value( id, amount )
    } else {
      console.error("Could not find an animal to donate to")
    }
  }

}
