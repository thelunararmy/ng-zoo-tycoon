# Zoo Tycoon

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.3.

## Install

```
git clone https://gitlab.com/thelunararmy/ng-zoo-tycoon
cd ng-zoo-tycoon
npm install
```

Requires `@angular/cli` to execute.


## Usage
```
ng serve
```

## Contributors

* [JC Bailey (@thelunararmy) ](@thelunaramy)

## Contribution

None. I am the boss.

## License

Noroff Accelerate, 2022.
